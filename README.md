# Gemnasium Gradle 7 Bug Demo


Run `./gradlew gemnasiumDumpDependencies` to get the error output:


```
> Task :gemnasiumDumpDependencies FAILED

FAILURE: Build failed with an exception.

* What went wrong:
A problem was found with the configuration of task ':gemnasiumDumpDependencies' (type 'DumpDependenciesTask').
  - Type 'DumpDependenciesTask' field 'TASK_NAME' without corresponding getter has been annotated with @Internal.
    
    Reason: Annotations on fields are only used if there's a corresponding getter for the field.
    
    Possible solutions:
      1. Add a getter for field 'TASK_NAME'.
      2. Remove the annotations on 'TASK_NAME'.
    
    Please refer to https://docs.gradle.org/7.0/userguide/validation_problems.html#ignored_annotations_on_field for more details about this problem.

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output. Run with --scan to get full insights.

* Get more help at https://help.gradle.org

Deprecated Gradle features were used in this build, making it incompatible with Gradle 8.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/7.0/userguide/command_line_interface.html#sec:command_line_warnings
```
